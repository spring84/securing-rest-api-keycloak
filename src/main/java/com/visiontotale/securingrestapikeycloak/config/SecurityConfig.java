package com.visiontotale.securingrestapikeycloak.config;

import com.visiontotale.securingrestapikeycloak.converter.JwtAuthConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
@RequiredArgsConstructor
public class SecurityConfig {

  private final JwtAuthConverter jwtAuthConverter;

  @Bean
  public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
    // We want all the requests to the API to be authenticated.
    http.csrf().disable().authorizeHttpRequests().anyRequest().authenticated();

    // Tell spring the resource server to use to validate the token that we will get as header.
    // The information of the resource server are configured in the application.yml file.
    http.oauth2ResourceServer().jwt().jwtAuthenticationConverter(jwtAuthConverter);

    // Define the session management policy
    http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

    return http.build();
  }
}
