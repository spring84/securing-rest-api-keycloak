package com.visiontotale.securingrestapikeycloak.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class APIRestController {

  @ApiOperation("Unprotected API")
  @GetMapping("/anonymous")
  public ResponseEntity<String> getAnonymous() {
    return ResponseEntity.ok("Hello Anonymous");
  }

  @ApiOperation("API accessible only by the user")
  @PreAuthorize("hasRole('client-user')")
  @GetMapping("/user")
  public ResponseEntity<String> getUser() {
    return ResponseEntity.ok("Hello User");
  }

  @ApiOperation("API accessible only by the admin")
  @PreAuthorize("hasRole('client-admin')")
  @GetMapping("/admin")
  public ResponseEntity<String> getAdmin() {
    return ResponseEntity.ok("Hello Admin");
  }

  @ApiOperation("API accessible both user and admin")
  @PreAuthorize("hasRole('client-admin') and hasRole('client-user')")
  @GetMapping("/all-user")
  public ResponseEntity<String> getAllUser() {
    return ResponseEntity.ok("Hello All User");
  }
}
